<?php

/**
 * @file
 * Contains site_send_message.page.inc.
 *
 * Page callback for Product entities.
 */

/**
 * Implements template_preprocess_site_send_message_successfully_issued().
 */
function template_preprocess_site_send_message_successfully_issued(&$variables) {
    // Загружаем конфигурацию.
    $config = \Drupal::config('site_send_message.settings');
    $variables['text_successfully_issued'] = $config->get('text_successfully_issued') ? t($config->get('text_successfully_issued')) : t("You will be contacted within 10 minutes to clarify the details.");
}
