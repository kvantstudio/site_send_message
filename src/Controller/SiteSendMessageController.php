<?php

namespace Drupal\site_send_message\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;

class SiteSendMessageController extends ControllerBase {
  /**
   * Возвращает заголовок страницы или стандартное уведомление.
   */
  public function getTitle(Request $request) {
    $string = $request->query->get('title');
    $string = trim($string);

    if ($string) {
      return $string;
    } else {
      return $this->t('Your message has been sent!');
    }
  }

  /**
   * Страница уведомления об успешной отправке сообщения на e-mail.
   */
  public function messageSuccessfullyIssued() {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_send_message_successfully_issued',
      '#attached' => [
        'library' => [
          'site_send_message/module',
        ],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'keys' => ['site_send_message_successfully_issued'],
        'tags' => ['site_send_message_successfully_issued'],
        'contexts' => ['languages'],
        'max-age' => Cache::PERMANENT,
      ],
    ];
  }

  /**
   * Последние зарегистрированные сообщения.
   */
  public function viewReportLastMessages(int $length = 1000) {
    $database = \Drupal::database();

    $query = $database->select('site_send_message', 'n');
    $query->fields('n');
    $query->range(0, $length);
    $query->orderBy('n.created', 'DESC');
    $result = $query->execute();

    $data = [];
    foreach ($result as $row) {
      $data[$row->mid] = [
        'uid' => $row->uid,
        'name' => $row->name,
        'phone' => $row->phone,
        'mail' => $row->mail,
        'mail_delivery' => $row->mail_delivery,
        'subject' => $row->subject,
        'text' => $row->text,
        'created' => \Drupal::service('date.formatter')->format($row->created, 'custom', 'd.m.Y - H:i'),
        'data' => empty($row->data) ? '' : Json::decode($row->data),
        'status' => $row->status,
      ];
    }

    return [
      '#theme' => 'site_send_message_view_last_reports_messages',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'site_send_message/module',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
