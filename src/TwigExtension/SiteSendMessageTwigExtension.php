<?php

namespace Drupal\site_send_message\TwigExtension;

use Drupal\site_send_message\Form\SiteSendMessageForm;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class SiteSendMessageTwigExtension extends AbstractExtension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return [
      new TwigFunction('get_send_message_form', $this->getSendMessageForm(...), ['is_safe' => ['html']]),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_send_message.twig_extension';
  }

  /**
   * Формирует HTML форму отправки сообщения.
   */
  public static function getSendMessageForm($subject = TRUE, $name = TRUE, $phone = TRUE, $mail = TRUE, $text = TRUE, $submit_label = 'Send', $title = 'Send message', $subject_default_value = 'Message from site', $text_placeholder = 'Your message', $id = 1) {
    $form = new SiteSendMessageForm($id);
    $form = \Drupal::formBuilder()->getForm($form, $subject, $name, $phone, $mail, $text, $submit_label, $title, $subject_default_value, $text_placeholder);
    return \Drupal::service('renderer')->render($form, FALSE);
  }
}
