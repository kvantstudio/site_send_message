<?php

/**
 * @file
 * Contains \Drupal\site_send_message\Form\SiteSendMessageSettingsForm
 */

namespace Drupal\site_send_message\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Subscription settings form.
 */
class SiteSendMessageSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_send_message_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_send_message.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config('site_send_message.settings');

    // Поле текст сообщения.
    $form['text_successfully_issued'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('The text on the page about the successful sending of the message'),
      '#description' => '',
      '#rows' => 2,
      '#default_value' => $config->get('text_successfully_issued') ? $config->get('text_successfully_issued') : "You will be contacted within 10 minutes to clarify the details.",
    );

    $form['cron_send_messages'] = array(
      '#title' => $this->t('Send messages only when Cron starts'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('cron_send_messages') ? 1 : 0,
    );

    // Объявляет поле количество писем для обработки за один запуск crone.
    $form['number'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Number of messages to be sent at the start Cron'),
      '#description' => $this->t('The recommended value is 10.'),
      '#default_value' => $config->get('number') ? $config->get('number') : 10,
    );

    $form['delete_messages'] = array(
      '#title' => $this->t('Delete all messages'),
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#description' => $this->t('Attention! If this value is selected, the data will be permanently deleted.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = \Drupal::service('config.factory')->getEditable('site_send_message.settings');
    $config->set('text_successfully_issued', trim($form_state->getValue('text_successfully_issued')));
    $config->set('cron_send_messages', $form_state->getValue('cron_send_messages'));
    $config->set('number', $form_state->getValue('number'));
    $config->save();

    // Удалить все сообщения.
    if ($form_state->getValue('delete_messages')) {
      $database = Database::getConnection();
      $query = $database->truncate('site_send_message')->execute();
      $this->messenger()->addStatus($this->t('All messages deleted.'));
    }
  }
}
