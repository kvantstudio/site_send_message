<?php

/**
 * @file
 * Contains \Drupal\site_send_message\Form\SiteSendMessageForm.
 */

namespace Drupal\site_send_message\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Send message form.
 */
class SiteSendMessageForm extends FormBase implements FormInterface {
  /**
   * {@inheritdoc}.
   */
  protected $type;

  /**
   * {@inheritdoc}.
   */
  public function __construct($type = "default") {
    $this->type = $type;
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_send_message_form_' . $this->type;
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $subject = TRUE, $name = TRUE, $phone = TRUE, $mail = TRUE, $text = TRUE, $submit_label = 'Send', $title = '', $subject_default_value = 'Message from site', $text_placeholder = 'Your message') {
    $account = \Drupal::currentUser();
    $config = $this->config('kvantstudio.settings');

    $form['#attributes']['class'][] = 'site-send-message-form';

    $form['contact_information'] = array(
      '#markup' => '<div class="site-send-message-form__title">' . $this->t($title) . '</div>',
      '#access' => $title ? TRUE : FALSE,
    );

    // Тема письма.
    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Message subject'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#default_value' => $this->t($subject_default_value),
      '#description' => '',
      '#attributes' => array('class' => array('site-send-message-form__input-item', 'site-send-message-form__subject'), 'placeholder' => $this->t('Message subject') . ' *'),
      '#suffix' => '<div id="site-send-message-form__validation-message-subject-' . $this->type . '" class="site-send-message-form__validation-message site-send-message-form__validation-message-subject hidden"></div>',
      '#access' => $subject ? TRUE : FALSE,
    );

    // Поле имя.
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#default_value' => $account->id() ? $account->getDisplayName() : "",
      '#description' => '',
      '#attributes' => array('class' => array('site-send-message-form__input-item', 'site-send-message-form__name'), 'placeholder' => $this->t('Your name') . ' *'),
      '#access' => $name && !$account->id() ? TRUE : FALSE,
      '#suffix' => '<div id="site-send-message-form__validation-message-name-' . $this->type . '" class="site-send-message-form__validation-message site-send-message-form__validation-message-name hidden"></div>',
    );

    // Поле телефон.
    $form['phone'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#default_value' => "",
      '#description' => '',
      '#attributes' => array('class' => array('site-send-message-form__input-item', 'site-send-message-form__phone'), 'placeholder' => $this->t('Phone number') . ' *'),
      '#access' => $phone && !$account->id() ? TRUE : FALSE,
      '#suffix' => '<div id="site-send-message-form__validation-message-phone-' . $this->type . '" class="site-send-message-form__validation-message site-send-message-form__validation-message-phone hidden"></div>',
    );

    // Поле e-mail.
    $form['mail_' . $this->type] = array(
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#default_value' => $account->id() ? $account->getEmail() : "",
      '#description' => '',
      '#attributes' => array('class' => array('site-send-message-form__input-item', 'site-send-message-form__mail'), 'placeholder' => $this->t('E-mail') . ' *'),
      '#access' => $mail && !$account->id() ? TRUE : FALSE,
      '#ajax' => [
        'callback' => '::validateEmailAjax',
        'event' => 'change',
        'progress' => array(
          'message' => NULL,
        ),
      ],
      '#suffix' => '<div id="site-send-message-form__validation-message-mail-' . $this->type . '" class="site-send-message-form__validation-message site-send-message-form__validation-message-mail hidden"></div>',
    );

    // Поле текст сообщения.
    $form['text'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Your message'),
      '#title_display' => 'invisible',
      '#description' => '',
      '#attributes' => array('class' => array('site-send-message-form__input-item', 'site-send-message-form__text'), 'placeholder' => $this->t($text_placeholder)),
      '#access' => $text ? TRUE : FALSE,
      '#rows' => 10,
    );

    // Соглашение об обработке персональных данных.
    if ($data_policy_information = $config->get('text_data_policy')) {
      $form['data_policy_' . $this->type] = array(
        '#type' => 'checkbox',
        '#attributes' => array('class' => array('site-send-message-form__data-policy')),
        '#default_value' => 1,
        '#prefix' => '<div id="site-send-message-form__data-policy-block-' . $this->type . '" class="site-send-message-form__data-policy-block">',
        '#ajax' => [
          'callback' => '::validateDataPolicy',
          'event' => 'change',
          'progress' => array(
            'message' => NULL,
          ),
        ],
      );

      $data_policy_information = str_replace("@submit_label", $this->t($submit_label), $data_policy_information);
      $nid = $config->get('node_agreement_personal_data');
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
      $data_policy_information = str_replace("@data_policy_url", $alias, $data_policy_information);
      $form['data_policy_information_' . $this->type] = array(
        '#markup' => $this->t($data_policy_information),
        '#suffix' => '</div>',
      );
    }

    // Добавляем нашу кнопку для отправки.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit_' . $this->type] = array(
      '#type' => 'submit',
      '#value' => $this->t($submit_label),
      '#button_type' => 'primary',
      '#attributes' => array('class' => array('site-send-message-form__submit', 'site-send-message-form__submit-' . $this->type)),
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
        'progress' => array(),
      ],
      '#suffix' => '<div id="site-send-message-form__submit-message-' . $this->type . '" class="site-send-message-form__submit-message hidden"></div>',
    );

    $form['#attached']['library'][] = 'site_send_message/module';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $mail = trim($form_state->getValue('mail_' . $this->type));
    if ($mail && !\Drupal::service('email.validator')->isValid($mail)) {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-mail-' . $this->type, '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' . $this->t('You do not specify the correct e-mail.')));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-mail-' . $this->type, 'removeClass', array('hidden')));
      $form_state->setValue('validate_error', TRUE);
    } else {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-mail-' . $this->type, ''));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-mail-' . $this->type, 'addClass', array('hidden')));
      $form_state->setValue('validate_error', FALSE);
    }

    // Если не указан e-mail, очищаем уведомление об ошибке.
    if (!$mail) {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-mail-' . $this->type, ''));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-mail-' . $this->type, 'addClass', array('hidden')));
      $form_state->setValue('validate_error', FALSE);
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateDataPolicy(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $data_policy = $form_state->getValue('data_policy_' . $this->type);
    if (!$data_policy) {
      $response->addCommand(new AlertCommand('Без согласия на обработку данных мы не сможем принять заявку.'));
      $response->addCommand(new InvokeCommand('.site-send-message-form__submit-' . $this->type, 'attr', array(['disabled' => true])));
    } else {
      $response->addCommand(new InvokeCommand('.site-send-message-form__submit-' . $this->type, 'attr', array(['disabled' => false])));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $account = \Drupal::currentUser();

    $config = $this->config('site_send_message.settings');

    // Выполняет стандартную валидацию полей формы и добавляет примечания об ошибках.
    FormBase::validateForm($form, $form_state);

    // Получаем значения полей.
    $subject = trim($form_state->getValue('subject'));
    if (!$subject) {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-subject-' . $this->type, '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' . $this->t('Please provide a subject')));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-subject-' . $this->type, 'removeClass', array('hidden')));
    } else {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-subject-' . $this->type, ''));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-subject-' . $this->type, 'addClass', array('hidden')));
    }

    $name = trim($form_state->getValue('name'));
    if (!$name) {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-name-' . $this->type, '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' . $this->t('Please provide a name')));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-name-' . $this->type, 'removeClass', array('hidden')));
    } else {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-name-' . $this->type, ''));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-name-' . $this->type, 'addClass', array('hidden')));
    }

    $phone = trim($form_state->getValue('phone'));
    if (!$phone) {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-phone-' . $this->type, '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' . $this->t('Please provide a phone')));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-phone-' . $this->type, 'removeClass', array('hidden')));
    } else {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-phone-' . $this->type, ''));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-phone-' . $this->type, 'addClass', array('hidden')));
    }

    $mail = trim($form_state->getValue('mail_' . $this->type));
    if (!$mail) {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-mail-' . $this->type, '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' . $this->t('Please provide a mail')));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-mail-' . $this->type, 'removeClass', array('hidden')));
    } else {
      $response->addCommand(new HtmlCommand('#site-send-message-form__validation-message-mail-' . $this->type, ''));
      $response->addCommand(new InvokeCommand('#site-send-message-form__validation-message-mail-' . $this->type, 'addClass', array('hidden')));
    }

    $text = trim($form_state->getValue('text'));

    $messages = \Drupal::messenger()->all();
    if (!$messages && !$form_state->getValue('validate_error')) {
      $connection = \Drupal::database();

      // Массив данных.
      $request_time = \Drupal::time()->getRequestTime();
      $fields = [
        'uid' => $account->id(),
        'form_name' => $this->type,
        'subject' => $subject,
        'name' => $name,
        'phone' => $phone,
        'mail' => $mail,
        'text' => $text,
        'status' => 0,
        'created' => $request_time,
      ];

      // Проверяем нужно ли отправить сообщение мгновенно.
      if ($config->get('cron_send_messages')) {
        site_send_message_create($fields);
      } else {
        site_send_message_create($fields, TRUE);
      }

      // Редирект на страницу подтверждения отправки.
      $route_name = 'site_send_message.successfully_issued';
      $response->addCommand(new RedirectCommand($this->url($route_name)));
    } else {
      $text = '<i class="fa fa-info" aria-hidden="true"></i> ' . $this->t('Fill in the correct form field.');
      $response->addCommand(new HtmlCommand('#site-send-message-form__submit-message-' . $this->type, $text));
    }

    return $response;
  }
}
