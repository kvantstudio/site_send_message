<?php

namespace Drupal\site_send_message\Plugin\rest\resource\v1;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Registers the message in the database and sends it to the e-mail.
 *
 * @RestResource(
 *   id = "site_send_message_send_message",
 *   label = @Translation("Registers the message in the database and sends it to the e-mail"),
 *   uri_paths = {
 *     "create" = "/api/v1/site-send-message/send-message",
 *   }
 * )
 */
class SendMessage extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new SendMessage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('site_send_message'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   */
  public function post($data) {
    try {
      $response['result'] = NULL;

      // Регистрация сообщения в БД.
      $fields = [
        'uid' => $this->currentUser->id(),
        'form_name' => empty($data['form_name']) ? NULL : trim($data['form_name']),
        'subject' => empty($data['subject']) ? NULL : trim($data['subject']),
        'name' => empty($data['name']) ? NULL : trim($data['name']),
        'phone' => empty($data['phone']) ? NULL : trim($data['phone']),
        'mail' => empty($data['mail']) ? NULL : trim($data['mail']),
        'text' => empty($data['text']) ? NULL : trim($data['text']),
        'data' => empty($data['data']) ? NULL : Json::encode($data['data']),
        'status' => 0,
        'created' => \Drupal::time()->getRequestTime(),
      ];
      $result = site_send_message_create($fields, TRUE);

      // Возвращаем данные.
      if ($result) {
        $response['status'] = TRUE;
      } else {
        $response['status'] = FALSE;
      }

      return new ModifiedResourceResponse($response);
    } catch (\Exception $e) {
      return new ResourceResponse('Check your data.', 400);
    }
  }
}
