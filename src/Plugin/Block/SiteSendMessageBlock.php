<?php

namespace Drupal\site_send_message\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a send message Block.
 *
 * @Block(
 *   id = "site_send_message_block",
 *   admin_label = @Translation("Send message"),
 * )
 */
class SiteSendMessageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return getSendMessageForm(TRUE,TRUE,TRUE,TRUE, 'Send', 'block');
  }

  /**
   * Формирует HTML форму отправки сообщения.
   */
  protected function getSendMessageForm($name = TRUE, $phone = TRUE, $mail = TRUE, $text = TRUE, $submit_label = 'Send', $id = 1) {
    $form = new SiteSendMessageForm($id);
    $form = \Drupal::formBuilder()->getForm($form, $name, $phone, $mail, $text, $submit_label);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
}